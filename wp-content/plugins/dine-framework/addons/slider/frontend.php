<?php
$class = array( 'dine-slider' );

$images = explode ( ',', $images );
$images = array_map( 'trim', $images );
if ( empty( $images ) ) return;

$options = array(
    'cellSelector' => '.carousel-cell',
    'wrapAround' => true,
    'prevNextButtons' => ( 'true' === $arrows ),
    'pageDots' => ( 'true' === $bullets ),
    'autoPlay' => ( 'true' == $autoplay ) ? absint( $autoplay_timer ) : false,
);

// CLASSES
$class = join( ' ', $class );

$attachments = get_posts( array(
    'posts_per_page' => -1,
    'orderby' => 'post__in',
    'post_type' => 'attachment',
    'post_status' => 'inherit',
    'post__in' => $images,
) );

if ( ! $attachments ) return;
?>
<div class="<?php echo esc_attr( $class ); ?>" data-options='<?php echo json_encode( $options ); ?>'>
    
    <div class="carousel">
        
        <?php foreach ( $attachments as $attachment ) : ?>

        <div class="carousel-cell">
            
            <?php echo wp_get_attachment_image( $attachment->ID, 'full' ); ?>
            
        </div><!-- .carousel-cell -->

        <?php endforeach; // attachments ?>

    </div><!-- .carousel -->

</div><!-- .dine-slider -->