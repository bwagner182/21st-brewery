<?php
$images = explode ( ',', $images );
$images = array_map( 'trim', $images );
if ( empty( $images ) ) return;

$gallery_class = array( 'dine-gallery' );

// Attachments
$attachments = get_posts( array(
    'posts_per_page' => -1,
    'orderby' => 'post__in',
    'post_type' => 'attachment',
    'post_status' => 'inherit',
    'post__in' => $images,
) );

$column = absint( $column );
if ( $column < 2 || $column > 6 ) $column = 3;
$gallery_class[] = 'column-' . $column;

// Ratio
if ( 'auto' != $ratio && 'square' != $ratio && 'portrait' != $ratio ) $ratio = 'landscape';
$gallery_class[] = 'gallery-' . $ratio;

if ( 'true' == $lightbox ) {
    $gallery_class[] = 'dine-lightbox-gallery';
}

$gallery_class = join( ' ', $gallery_class );
?>

<div class="<?php echo esc_attr( $gallery_class ); ?>">
    
    <?php foreach ( $attachments as $attachment ) : ?>

    <figure class="dine-gallery-item">
        
        <div class="dine-gallery-item-inner">
    
            <?php
            if ( 'true' == $lightbox ) {

                $title = trim( $attachment->post_title );
                $img_caption = trim( $attachment->post_excerpt );

                $fullsize = wp_get_attachment_image_src( $attachment->ID, 'full' );

                $open = '<a href="' . esc_url( $fullsize[0] ) . '" class="lightbox-link">';
                $close = '</a>';
            } else {
                $open = $close = '';
            }

            $img = wp_get_attachment_image( $attachment->ID, 'medium' );

            ?>

            <?php echo $open . $img . $close ; ?>

            <div class="height-element"></div>

            <div class="loading-icon"></div>
            
            <?php if ( 'true' == $caption && $img_caption ) { ?>
            
            <span class="gallery-item-caption"><?php echo $img_caption; ?></span>
            
            <?php } // caption ?>
            
        </div>
    
    </figure><!-- .dine-gallery-item -->
    
    <?php endforeach; // $attachments ?>

</div><!-- .dine-gallery -->