<?php
$html = '';

echo '<div class="dine-menu-wrapper">';

if ( $title ) echo '<h2 class="dine-menu-heading">' . $title  .'</h2>';

echo '<div class="dine-menu">';

$items = vc_param_group_parse_atts( $items );
if ( $items && is_array( $items ) ) {

    foreach ( $items as $item ) {
        
        extract( wp_parse_args( $item, array(
            'price' => '',
            'name' => '',
            'desc' => '',
        ) ) );
        
        ?>

<div class="dine-menu-item">
    
    <h3 class="menu-item-name"><?php echo $name; ?></h3>
    
    <span class="menu-item-price"><?php echo $price; ?></span>
    
    <div class="menu-item-desc">
    
        <?php echo do_shortcode( $desc ); ?>
    
    </div>
    
</div><!-- .dine-menu-item -->


<?php }
    
}

echo '</div></div>';