<?php
$all_navs = array( '' => esc_html__( 'Select Navigation', 'dine' ) );
$all_navs = $all_navs + get_terms( 'nav_menu', array( 'hide_empty' => true, 'fields' =>  'id=>name' ) );
$all_navs = array_flip( $all_navs );

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Navigation',
    'description' => 'To create a new navigation, please go to Dashboard > Appearance > Menus',
    'param_name' => 'nav',
    'value' => $all_navs,
    'std' => '',
    'admin_label' => true,
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Layout',
    'param_name' => 'layout',
    'value' => array( 
        'Horizontal' => 'horizontal',
        'Vertical' => 'vertical'
    ),
    'std' => 'horizontal',
    'admin_label' => true,
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Align',
    'param_name' => 'align',
    'value' => array( 
        'Left' => 'left',
        'Center' => 'center',
        'Right' => 'right',
    ),
    'std' => 'left',
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Size',
    'param_name' => 'size',
    'value' => array( 
        'Normal' => 'normal',
        'Medium' => 'medium',
        'Large' => 'large',
        'Giantic' => 'gigantic',
    ),
    'std' => 'normal',
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Between items',
    'param_name' => 'separator',
    'value' => array( 
        'Empty Space' => 'space',
        'Slash (/)' => 'slash',
        'Bullet' => 'dot',
    ),
    'std' => 'space',
    
    'dependency' => array(
        'element' => 'layout',
        'value' => 'horizontal',
    ),
);

$params[] = array(
    'type' => 'colorpicker',
    'heading' => 'Item Color',
    'param_name' => 'color',
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Hover/Active Style',
    'param_name' => 'active_style',
    'value' => array(
        'Underline'     => 'underline',
        'Line Through'  => 'line-through',
        'Custom Color'  => 'color',
        'Underline & Custom Color' => 'underline-color',
        'Opacity'       => 'opacity',
    ),
    'std'               => 'underline',
);

$params[] = array(
    'type' => 'colorpicker',
    'heading' => 'Active Link Color',
    'param_name' => 'active_color',
    
    'dependency' => array(
        'element' => 'active_style',
        'value' => array( 'color', 'underline-color' ),
    ),
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Line Thickness',
    'param_name' => 'border',
    'value' => array(
        '1px' => '1px',
        '2px' => '2px',
        '3px' => '3px',
        '4px' => '4px',
        '5px' => '5px',
        '6px' => '6px',
    ),
    'std' => '1px',
    
    'dependency' => array(
        'element' => 'active_style',
        'value' => array( 'underline', 'underline-color' ),
    ),
);

// Gallery
//
$params[] = array(
    'type' => 'attach_images',
    'heading' => 'Upload Slider Images',
    'description' => 'Slide images will appear in order of menu items.',
    'param_name' => 'images',
    
    'group' => 'Slideshow',
);

$params[] = array(
    'type' => 'attach_image',
    'heading' => 'Default Image',
    'description' => 'Default Image',
    'param_name' => 'default_image',
    
    'group' => 'Slideshow',
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Slider Height',
    'value'     => array(
        'Custom height' => 'custom',
        'Fullscreen' => 'fullscreen',
    ),
    'param_name' => 'height',
    
    'group' => 'Slideshow',
);

$params[] = array(
    'type' => 'textfield',
    'heading' => 'Custom Height',
    'value'     => '500',
    'param_name' => 'custom_height',
    'dependency' => array(
        'element'   => 'height',
        'value'     => 'custom',
    ),
    
    'group' => 'Slideshow',
);

$params[] = array(
    'type' => 'dropdown',
    'heading' => 'Navigation Position',
    'value'   => array(
        'Top Left' => 'top left',
        'Top Center' => 'top center',
        'Top Right' => 'top right',
        
        'Middle Left' => 'middle left',
        'Middle Center' => 'middle center',
        'Middle Right' => 'middle right',
        
        'Bottom Left' => 'bottom left',
        'Bottom Center' => 'bottom center',
        'Bottom Right' => 'bottom right',
    ),
    'std' => 'middle center',
    'param_name' => 'nav_position',
    
    'group' => 'Slideshow',
);