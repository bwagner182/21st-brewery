<?php
if ( ! class_exists( 'Dine_Nav_Shortcode' ) ) :
/**
 * Custom Nav for Visual Composer
 *
 * @since 1.0
 */
class Dine_Nav_Shortcode extends Dine_Shortcode
{
    
    public function __construct() {
        
        $this->path = DINE_FRAMEWORK_PATH . 'addons/nav/';
        $this->args = array(
            'base'      => 'nav',
            'name'      => esc_html__( 'Custom Navigation', 'dine' ),
            'desc'      => esc_html__( 'Displays a custom navigation menu', 'dine' ),
            'weight'    => 190,
        );
        
    }
    
    function param_list() {
        return 'nav, layout, align, size, separator, color, active_style, active_color, border, images, height, custom_height, nav_position, default_image';
    }
    
}

$instance = new Dine_Nav_Shortcode();
$instance->init();

endif;