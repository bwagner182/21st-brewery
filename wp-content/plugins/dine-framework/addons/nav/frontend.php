<?php
if ( ! $nav ) return;
$id = 'custom-nav-' . rand( 0, 10000 );

$class = array( 'dine-custom-nav' );
$wrapper_class = array( 'dine-custom-nav-wrapper' );
$nav_css = array();

// layout
//
if ( 'vertical' != $layout ) $layout = 'horizontal';
$class[] = 'dine-nav-' . $layout;

// align
//
if ( 'center' != $align && 'right' != $align ) $align = 'left';
$class[] = 'dine-nav-' . $align;

// size
//
if ( 'medium' != $size && 'large' != $size && 'gigantic' != $size ) $size = 'normal';
$class[] = 'dine-nav-' . $size;

// Separator
//
if ( 'horizontal' == $layout ) {
    if ( 'dot' != $separator && 'slash' != $separator ) $separator = 'space';
    $class[] = 'dine-nav-separator-' . $separator;
}

// Active Style
//
if ( 'line-through' != $active_style && 'underline-color' != $active_style && 'color' != $active_style && 'opacity' != $active_style ) $active_style = 'underline';
if ( 'underline-color' == $active_style ) {
    $class[] = 'dine-nav-active-style-underline';
    $class[] = 'dine-nav-active-style-color';
} else {
    $class[] = 'dine-nav-active-style-' . $active_style;
}
if ( ( 'color' == $active_style || 'underline-color' == $active_style ) && $active_color ) {
    echo '<style>';
    echo "#{$id} a:hover, #{$id} li.current-menu-item a { color: {$active_color}; }";
    echo '</style>';
}

$border_style = '';
if ( 'underline-color' == $active_style || 'underline' == $active_style ) {
    if ( $border ) $border_style = ' style="border-bottom-width:' . $border . ';"';
}

// Custom Color
//
if ( $color ) {
    $nav_css[] = 'color:' . $color .'';
}
$nav_css = join( ';', $nav_css );
if ( $nav_css ) $nav_css = ' style="' . esc_attr( $nav_css ). '"';

// Render
//
$nav_render = wp_nav_menu( array( 'menu' => $nav, 'link_before' => '<span>', 'link_after' => '</span><span class="nav-item-border"' . $border_style . '></span>', 'depth' => 1, 'echo' => false, 'fallback_cb' => false  ) );
if ( ! $nav_render ) return;

// Images
//
$nav_obj = wp_get_nav_menu_object( $nav ); /* To limit number of photos to retrieve */
$images = explode ( ',', $images );
if ( $default_image ) {
    $images[] = $default_image;
}
$images = array_map( 'trim', $images );
if ( ! empty( $images ) ) {

    $attachments = get_posts( array(
        'posts_per_page' => $nav_obj ? ( $nav_obj->count + 1 ) : -1,
        'orderby' => 'post__in',
        'post_type' => 'attachment',
        'post_status' => 'inherit',
        'post__in' => $images,
    ) );
    
    ob_start();
    foreach ( $attachments as $attachment ) {
    
        ?>
        <div class="rsContent">
            
            <?php echo wp_get_attachment_image( $attachment->ID, 'full', false, array( 'class' => 'rsImg' ) ); ?>

        </div><!-- .rsContent -->
    
    <?php
    }
    $slider = ob_get_clean();
    
    if ( $slider ) {
        
        $slider_class = array(
            'dine-royalSlider' 
        );
        $slider_options = array(
            'transitionType' => 'fade',
            'transitionSpeed' => 600,
            'arrowsNav' => false,
            'controlNavigation' => 'none',
            'sliderDrag' => false,
            'navigateByClick' => false,
            'autoPlay' => array(
                'enabled' => false,
            ),
            'imageScaleMode' => 'fill',
            'startSlideId' => $nav_obj->count,
        );
        
        // Slider Height
        //
        $height_css = '';
        if ( 'fullscreen' == $height ) {
            $slider_class[] = 'slider-fullscreen';
        } else {
            $custom_height = strtolower( $custom_height );
            if ( substr( $custom_height, -2, 2 ) === 'vh' ) {
                $height_css = ' style="height:' . $custom_height . ';"';
            } else {
                $custom_height = absint( $custom_height );
                if ( $custom_height > 10 ) {
                    $height_css = ' style="height:' . $custom_height . 'px;"';
                }
            }
        }
        
        $slider_class = join( ' ', $slider_class );
        
        $slider = '<div class="' . esc_attr( $slider_class ) . '" data-options=\'' . json_encode( $slider_options ) . '\'><div class="royalSlider rsDine"' . $height_css .'>'
            . $slider . '</div></div>';
        
        $wrapper_class[] = 'dine-nav-slider';
        
        // Nav Position
        $nav_position = str_replace( ' ', ' nav-position-', $nav_position );
        $nav_position = 'nav-position-' . $nav_position;
        $wrapper_class[] = $nav_position;
    
    }
    
}

$class = join( ' ', $class );
$wrapper_class = join( ' ', $wrapper_class );
?>

<div class="<?php echo esc_attr( $wrapper_class ); ?>">
    
    <div class="<?php echo esc_attr( $class ); ?>" id="<?php echo esc_attr( $id ); ?>"<?php echo $nav_css; ?>>

        <?php echo $nav_render; ?>

    </div><!-- .dine-nav -->
    
    <?php echo $slider; ?>
    
</div>